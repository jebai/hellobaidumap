//
//  main.m
//  HelloBaiduMap
//
//  Created by mingchen on 8/2/14.
//  Copyright (c) 2014 森云软件. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
