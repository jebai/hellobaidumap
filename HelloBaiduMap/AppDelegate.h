//
//  AppDelegate.h
//  HelloBaiduMap
//
//  Created by mingchen on 8/2/14.
//  Copyright (c) 2014 森云软件. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, BMKGeneralDelegate>
{
    BMKMapManager* _mapManager;
}

@property (strong, nonatomic) UIWindow *window;

@end
