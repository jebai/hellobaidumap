//
//  ViewController.m
//  HelloBaiduMap
//
//  Created by mingchen on 8/2/14.
//  Copyright (c) 2014 森云软件. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <BMKMapViewDelegate>
{
    BMKMapView* _mapView;
    BMKAnnotationView* newAnnotation;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    
    _mapView = [[BMKMapView alloc]initWithFrame:self.view.bounds];
    [_mapView setShowMapScaleBar:YES];
    
    CLLocationCoordinate2D zoo = (CLLocationCoordinate2D){32.09893, 118.813615};
    CLLocationCoordinate2D nanqi = (CLLocationCoordinate2D){32.043409, 118.78436699999997};

    // 将google坐标转换成百度坐标
    nanqi = BMKCoorDictionaryDecode(BMKConvertBaiduCoorFrom(nanqi, BMK_COORDTYPE_COMMON));
    
    BMKPointAnnotation* nanqiPointAnnotation = [[BMKPointAnnotation alloc]init];
    nanqiPointAnnotation.coordinate = nanqi;
    nanqiPointAnnotation.title = @"南汽技工学校";
    nanqiPointAnnotation.subtitle = @"南汽技工学校!";
    [_mapView addAnnotation:nanqiPointAnnotation];
    
    BMKPointAnnotation* zooPointAnnotation = [[BMKPointAnnotation alloc]init];
    zooPointAnnotation.coordinate = zoo;
    zooPointAnnotation.title = @"红山森林动物园";
    zooPointAnnotation.subtitle = @"红山森林动物园!";
    [_mapView addAnnotation:zooPointAnnotation];
    
    // 选中zoo,弹出小气泡
    [_mapView selectAnnotation:nanqiPointAnnotation animated:YES];
    
    _mapView.centerCoordinate = nanqi;
    _mapView.zoomLevel = 18;
    [self.view addSubview:_mapView];
}

// 根据anntation生成对应的View
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation
{
    NSString *AnnotationViewID = @"renameMark";
    if (newAnnotation == nil) {
        newAnnotation = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
        // 设置颜色
		((BMKPinAnnotationView*)newAnnotation).pinColor = BMKPinAnnotationColorPurple;
        // 从天上掉下效果
		((BMKPinAnnotationView*)newAnnotation).animatesDrop = YES;
        // 设置可拖拽
		((BMKPinAnnotationView*)newAnnotation).draggable = NO;
    }
    return newAnnotation;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [_mapView viewWillAppear];
    _mapView.delegate = self; // 此处记得不用的时候需要置nil，否则影响内存的释放
}

-(void)viewWillDisappear:(BOOL)animated
{
    [_mapView viewWillDisappear];
    _mapView.delegate = nil; // 不用时，置nil
}

@end
